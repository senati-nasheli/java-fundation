import java.util.Scanner;

public class ejercicio6 {
  public static void main(String[] args) {
    Scanner pollito = new Scanner(System.in);

    System.out.print("Ingrese un numero: ");
    int num1 = pollito.nextInt();

    System.out.print("Ingrese otro numero: ");
    int num2 = pollito.nextInt();

    int cociente = num1 / num2;
    int resto = num1 % num2;

    System.out.println("El cociente de " + num1 + " entre " + num2 + " es: " + cociente);
    System.out.println("El resto de " + num1 + " entre " + num2 + " es: " + resto);
  }
}
