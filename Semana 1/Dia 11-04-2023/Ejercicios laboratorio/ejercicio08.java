import java.util.Scanner;

public class ejercicio8 {
    public static void main(String[] args) {
        Scanner numlados = new Scanner(System.in);
        System.out.print("Longitud lado 1: ");
        double lado1 = numlados.nextDouble();

        System.out.print("Longitud lado 2: ");
        double lado2 = numlados.nextDouble();

        System.out.print("Longitud lado 3: ");
        double lado3 = numlados.nextDouble();

        String resultado = obtenerTipoTriangulo(lado1, lado2, lado3);

        System.out.println("El triangulo es " + resultado);
    }

    public static String obtenerTipoTriangulo(double lado1, double lado2, double lado3) {
        if (lado1 == lado2 && lado2 == lado3) {
            return "Equilátero";
        } else if (lado1 == lado2 || lado2 == lado3 || lado1 == lado3) {
            return "Isósceles";
        } else {
            return "Escaleno";
        }
        
    }
}
