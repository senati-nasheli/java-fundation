import java.util.Scanner;

public class ejercicio11 {

    public static void main(String[] args) {
        Scanner idkdc = new Scanner(System.in);
        
        System.out.print("Ingresa el primer número entero: ");
        int num1 = idkdc.nextInt();
        
        System.out.print("Ingresa el segundo número entero: ");
        int num2 = idkdc.nextInt();
        
        if (num1 > num2) {
            System.out.println("El primer número es mayor que el segundo.");
        } else if (num1 < num2) {
            System.out.println("El segundo número es mayor que el primero.");
        } else {
            System.out.println("Los dos números son iguales.");
        }
        
    }

}
