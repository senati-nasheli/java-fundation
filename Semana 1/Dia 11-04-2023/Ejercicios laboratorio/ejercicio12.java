import java.util.Scanner;

public class ejercicio12 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        
        System.out.print("Ingresa un número entero: ");
        int n1 = sc.nextInt();
        
        System.out.print("Ingresa otro número entero: ");
        int n2 = sc.nextInt();
        
        if (n1 == n2) {
            System.out.println("Los dos números son iguales.");
        } else {
            System.out.println("Los dos números son diferentes.");
        }
    }
}
