import java.util.Scanner;

public class ejercicio17 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        
        System.out.print("Ingresa un número: ");
        int num = sc.nextInt();
        
        if (num > 0) {
            System.out.println(num + " es positivo.");
        } else if (num < 0) {
            System.out.println(num + " es negativo.");
        } else {
            System.out.println("El número es cero.");
        }
    }
}
