import java.util.Scanner;
  
public class ejercicio20 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        
        System.out.print("Ingrese un numero: ");
        int num1 = sc.nextInt();
        
        System.out.print("Ingrese otro numero: ");
        int num2 = sc.nextInt();
        
        int mcd = calcularMCD(num1, num2);
        int mcm = (num1 * num2) / mcd;
        
        System.out.println("El Máximo Común Divisor es " + mcd);
        System.out.println("El Mínimo Común Múltiplo es " + mcm);
        
        sc.close();
    }
    
    public static int calcularMCD(int num1, int num2) {
        if (num2 == 0) {
            return num1;
        }
        
        return calcularMCD(num2, num1 % num2);
    }
}
