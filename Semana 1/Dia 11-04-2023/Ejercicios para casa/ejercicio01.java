import java.util.ArrayList;

public class ejercicio01 {
    public static void main(String[] args) {
        ArrayList<String> nombres = new ArrayList<String>();
        nombres.add("Jose");
        nombres.add("Martha");
        nombres.add("Piero");
        nombres.add("Luisa");
        nombres.add("Belos");

        System.out.println("Lista de nombres:");
        for (String nombre : nombres) {
            System.out.println(nombre);
        }
    }
    
}