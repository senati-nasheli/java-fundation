import java.util.HashMap;

public class ejercicio02 {
    public static void main(String[] args) {
        HashMap<String, Integer> personas = new HashMap<String, Integer>();
        personas.put("Maria", 25);
        personas.put("Moises", 30);
        personas.put("Pedro", 40);
        personas.put("Alicia", 22);
        personas.put("Luigui", 33);

        System.out.println("Lista de personas:");
        for (String nombre : personas.keySet()) {
            System.out.println(nombre + " tiene " + personas.get(nombre) + " años.");
        }
    }
}
