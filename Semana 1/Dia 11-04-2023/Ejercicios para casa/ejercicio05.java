import java.io.File;

public class ejercicio5 {
    public static void main(String[] args) {
        String rutaArchivo = "/ruta/del/archivo.txt";
        File archivo = new File(rutaArchivo);

        System.out.println("Ruta del archivo: " + archivo.getAbsolutePath());
    }
}
