public class ejercicio09 {
    public static void main(String[] args) {
    double a = 5.0;
    double b = 2.0;

    double resultado1 = Math.sqrt(a); 
    double resultado2 = Math.pow(a, b);
    double resultado3 = Math.max(a, b); 
    
    System.out.println("Resultado 1: " + resultado1);
    System.out.println("Resultado 2: " + resultado2);
    System.out.println("Resultado 3: " + resultado3);
    }
}
