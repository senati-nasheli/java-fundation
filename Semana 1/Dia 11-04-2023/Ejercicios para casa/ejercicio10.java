import java.util.regex.Matcher;
import java.util.regex.Pattern;
public class ejercicio10 {
    public static void main(String[] args) {
        String texto = "Java es un lenguaje de programación muy popular.";
        String regex = ".*java.*";
        Pattern pattern = Pattern.compile(regex, Pattern.CASE_INSENSITIVE);
        Matcher matcher = pattern.matcher(texto);
        boolean encontrado = matcher.find();
        if (encontrado) {
            System.out.println("Se encontró una coincidencia.");
        } else {
            System.out.println("No se encontró ninguna coincidencia.");
        }
    }
}
