import java.util.regex.Matcher;
import java.util.regex.Pattern;
public class ejercicio11 {
    public static void main(String[] args) {
        String texto = "Java es un lenguaje de programación muy popular. Java es utilizado en muchos proyectos.";
        Pattern pattern = Pattern.compile("Java");
        Matcher matcher = pattern.matcher(texto);
        while (matcher.find()) {
            System.out.println("Se encontró una ocurrencia en la posición " + matcher.start());
        }
    }
}
