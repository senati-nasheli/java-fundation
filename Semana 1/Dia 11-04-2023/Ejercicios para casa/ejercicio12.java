import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
public class ejercicio12 {
    public static void main(String[] args) {
        DateFormat formatoFecha = new SimpleDateFormat("dd/MM/yyyy");
        Date fecha = new Date();
        String fechaFormateada = formatoFecha.format(fecha);
        System.out.println("La fecha formateada es: " + fechaFormateada);
        
        DateFormat formatoHora = new SimpleDateFormat("HH:mm:ss");
        Date hora = new Date();
        String horaFormateada = formatoHora.format(hora);
        System.out.println("La hora formateada es: " + horaFormateada);
    }
}
