import java.text.SimpleDateFormat;
import java.util.Date;
public class ejercicio13 {
    public static void main(String[] args) {
        SimpleDateFormat formatoFecha = new SimpleDateFormat("dd-MMM-yyyy");
        Date fecha = new Date();
        String fechaFormateada = formatoFecha.format(fecha);
        System.out.println("La fecha formateada es: " + fechaFormateada);
        
        SimpleDateFormat formatoHora = new SimpleDateFormat("HH:mm:ss");
        Date hora = new Date();
        String horaFormateada = formatoHora.format(hora);
        System.out.println("La hora formateada es: " + horaFormateada);
    }
}
