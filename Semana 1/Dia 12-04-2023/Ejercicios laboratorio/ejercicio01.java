import java.util.Scanner;

public class ejercicio01 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.print("Ingresa un número entero: ");
        int num = sc.nextInt();
        
        if (num % 2 == 0) {
            System.out.println(num + "es par");
        } 
        else {
            System.out.println(num + "es impar");
        }
    }
}
