import java.util.Scanner;
public class ejercicio04 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.println("Ingresa tres números enteros: ");
        int lado1 = sc.nextInt();
        int lado2 = sc.nextInt();
        int lado3 = sc.nextInt();
        
        if (lado1 + lado2 > lado3 && lado1 + lado3 > lado2 && lado2 + lado3 > lado1) {
            System.out.println("Los tres números pueden formar un triángulo válido.");
        } else {
            System.out.println("Los tres números no pueden formar un triángulo válido.");
        }
    }
}
