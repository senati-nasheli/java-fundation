import java.util.Scanner;
public class ejercicio06 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.print("Ingresa tu peso: ");
        double peso = sc.nextDouble();
        System.out.print("Ingresa tu estatura en metros: ");
        double altura = sc.nextDouble();

        double imc = peso / (altura * altura);
        System.out.println("Tu IMC es de: " + imc);
    }
}
