import java.util.Scanner;

public class ejercicio07 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.print("Ingresa una cadena de caracteres: ");
        String cadena = sc.nextLine();
        
        cadena = cadena.replaceAll("\\s", "").toLowerCase();
        
        String cadenaInvertida = new StringBuilder(cadena).reverse().toString();
        if (cadena.equals(cadenaInvertida)) {
            System.out.println("La cadena es un palíndromo.");
        } else {
            System.out.println("La cadena no es un palíndromo.");
        }
    }
}
