import java.util.Scanner;

public class ejercicio08 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.print("Ingresa una fecha en formato dd/mm/yyyy: ");
        String fecha = sc.nextLine();
        
        // Separamos el día, mes y año utilizando el carácter "/"
        String[] partes = fecha.split("/");
        
        // Convertimos cada parte a un número entero
        int dia = Integer.parseInt(partes[0]);
        int mes = Integer.parseInt(partes[1]);
        int anio = Integer.parseInt(partes[2]);
        
        // Verificamos si la fecha es válida
        boolean fechaValida = true;
        if (anio < 1 || anio > 9999) {
            fechaValida = false;
        } else if (mes < 1 || mes > 12) {
            fechaValida = false;
        } else {
            int[] diasPorMes = {31,28,31,30,31,30,31,31,30,31,30,31};
            if (esBisiesto(anio)) {
                diasPorMes[1] = 29;
            }
            if (dia < 1 || dia > diasPorMes[mes-1]) {
                fechaValida = false;
            }
        }
        
        // Imprimimos el resultado
        if (fechaValida) {
            System.out.println("La fecha es válida.");
        } else {
            System.out.println("La fecha no es válida.");
        }
    }
    
    // Función auxiliar que determina si un año es bisiesto o no
    public static boolean esBisiesto(int anio) {
        return (anio % 4 == 0 && anio % 100 != 0) || anio % 400 == 0;
    }
}
