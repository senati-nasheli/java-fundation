import java.util.Scanner;

public class ejercicio10 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.print("Ingresa una lista de números enteros separados por espacios: ");
        String numeros = sc.nextLine();
    
        int pares = 0;
        String[] numerosSeparados = numeros.split(" ");
        for (String numero : numerosSeparados) {
            int n = Integer.parseInt(numero);
            if (n % 2 == 0) {
                pares++;
            }
        }
        
        System.out.println("Hay " + pares + " números pares en la lista.");
    }
}
