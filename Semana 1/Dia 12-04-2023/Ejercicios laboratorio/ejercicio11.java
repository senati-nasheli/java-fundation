import java.util.Scanner;

public class ejercicio11 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.print("Ingresa una lista de números enteros separados por espacios: ");
        String numeros = sc.nextLine();
        
        // Separamos los números y contamos los múltiplos de 3
        int multiplos = 0;
        String[] numerosSeparados = numeros.split(" ");
        for (String numero : numerosSeparados) {
            int n = Integer.parseInt(numero);
            if (n % 3 == 0) {
                multiplos++;
            }
        }
        
        // Imprimimos el resultado
        System.out.println("Hay " + multiplos + " números múltiplos de 3 en la lista.");
    }
}
