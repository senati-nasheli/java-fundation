import java.util.Scanner;

public class ejercicio12 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.print("Ingresa una lista de números enteros separados por espacios: ");
        String numeros = sc.nextLine();
        
        int perfectos = 0;
        String[] numerosSeparados = numeros.split(" ");
        for (String numero : numerosSeparados) {
            int n = Integer.parseInt(numero);
            if (esPerfecto(n)) {
                perfectos++;
            }
        }
        
        System.out.println("Hay " + perfectos + " números perfectos en la lista.");
    }
    
    public static boolean esPerfecto(int n) {
        int suma = 0;
        for (int i = 1; i < n; i++) {
            if (n % i == 0) {
                suma += i;
            }
        }
        return suma == n;
    }
}
