import java.util.Scanner;

public class ejercicio13 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.print("Ingresa una lista de números enteros separados por espacios: ");
        String numeros = sc.nextLine();
        
        int fib = 0, prev1 = 0, prev2 = 1, n;
        int fibCount = 0;
        String[] numerosSeparados = numeros.split(" ");
        for (String numero : numerosSeparados) {
            n = Integer.parseInt(numero);
            fib = 0;
            prev1 = 0;
            prev2 = 1;
            while (fib < n) {
                fib = prev1 + prev2;
                prev1 = prev2;
                prev2 = fib;
            }
            if (fib == n) {
                fibCount++;
            }
        }
        
        System.out.println("Hay " + fibCount + " números de Fibonacci en la lista.");
    }
}
