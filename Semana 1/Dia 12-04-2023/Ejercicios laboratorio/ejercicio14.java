import java.util.Scanner;

public class ejercicio14 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.print("Ingresa una lista de palabras separadas por espacios: ");
        String palabras = sc.nextLine();
        
        int palindromoConta = 0;
        String[] palabrasSeparadas = palabras.split(" ");
        for (String palabra : palabrasSeparadas) {
            if (esPalindromo(palabra)) {
                palindromoConta++;
            }
        }
        
        System.out.println("Hay " + palindromoConta + " palíndromos en la lista.");
    }
    
    public static boolean esPalindromo(String palabra) {
        int longitud = palabra.length();
        for (int i = 0; i < longitud/2; i++) {
            if (palabra.charAt(i) != palabra.charAt(longitud-1-i)) {
                return false;
            }
        }
        return true;
    }
}
