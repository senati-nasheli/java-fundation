import java.util.Scanner;

public class ejercicio15 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.print("Ingresa una lista de palabras separadas por espacios: ");
        String palabras = sc.nextLine();
        System.out.print("Ingresa la letra que quieres buscar: ");
        char letra = sc.nextLine().charAt(0);
        
        int count = 0;
        String[] palabrasSeparadas = palabras.split(" ");
        for (String palabra : palabrasSeparadas) {
            if (palabra.charAt(0) == letra) {
                count++;
            }
        }
        
        System.out.println("Hay " + count + " palabras que comienzan con la letra " + letra + ".");
    }
}
