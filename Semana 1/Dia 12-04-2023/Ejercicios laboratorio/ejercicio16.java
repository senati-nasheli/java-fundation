import java.util.Scanner;

public class ejercicio16 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.print("Ingresa una lista de palabras separadas por espacios: ");
        String palabras = sc.nextLine();
        System.out.print("Ingresa la letra que quieres buscar: ");
        char letra = sc.nextLine().charAt(0);
        
        // Separamos las palabras y contamos las que contienen la letra indicada
        int count = 0;
        String[] palabrasSeparadas = palabras.split(" ");
        for (String palabra : palabrasSeparadas) {
            if (palabra.indexOf(letra) != -1) {
                count++;
            }
        }
        
        System.out.println("Hay " + count + " palabras que contienen la letra " + letra);
    }
}
