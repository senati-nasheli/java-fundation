import java.util.Scanner;

public class ejercicio18 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.print("Ingresa una lista de palabras separadas por espacios: ");
        String palabras = sc.nextLine();
        System.out.print("Ingresa la longitud máxima de las palabras que deseas contar: ");
        int longitud = sc.nextInt();
        
        int count = 0;
        String[] palabrasSeparadas = palabras.split(" ");
        for (String palabra : palabrasSeparadas) {
            if (palabra.length() < longitud) {
                count++;
            }
        }
        
        System.out.println("Hay " + count + " palabras con longitud menor a " + longitud + ".");
    }
}
