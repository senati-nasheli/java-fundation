import java.util.Scanner;
public class ejercicio19 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.print("Ingresa un número entero: ");
        int numero = sc.nextInt();
        
        double suma = 0.0;
        for (int i = 1; i <= numero; i++) {
            suma += 1.0 / i;
        }
        
        if (suma == Math.round(suma)) {
            System.out.println(numero + " es un número armónico.");
        } else {
            System.out.println(numero + " no es un número armónico.");
        }
    }
}
