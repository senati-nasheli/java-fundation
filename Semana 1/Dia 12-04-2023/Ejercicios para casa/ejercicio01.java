import java.util.ArrayList;
import java.util.List;

public class ejercicio01 {
    public static void main(String[] args) {
        List<Integer> numeros = new ArrayList<Integer>();
        numeros.add(1);
        numeros.add(2);
        numeros.add(3);
        numeros.add(4);
        numeros.add(5);
        numeros.add(6);
        numeros.add(7);
        numeros.add(8);
        numeros.add(9);
        numeros.add(10);

        int sumaBuscada = 10;

        for(int i=0; i<numeros.size(); i++) {
            for(int j=i+1; j<numeros.size(); j++) {
                int num1 = numeros.get(i);
                int num2 = numeros.get(j);
                if(num1 + num2 == sumaBuscada && !numeros.subList(0,i).contains(num1) && !numeros.subList(0,i).contains(num2)) {
                    System.out.println("El par de numeros " + num1 + " y " + num2 + " suma " + sumaBuscada);
                    break;
                }
            }
        }

    }
}
