import java.util.ArrayList;
import java.util.List;

public class ejercicio02 {
    public static void main(String[] args) {
        List<String> palabras = new ArrayList<String>();
        palabras.add("avion");
        palabras.add("ramo");
        palabras.add("teatro");
        palabras.add("aldea"); 
        palabras.add("mueble");
        palabras.add("fuente");
        
        int contador = 0;
        
        for(String palabra : palabras) {
            int vocalesConsecutivas = 0;
            for(int i=0; i<palabra.length(); i++) {
                char letra = palabra.charAt(i);
                if(esVocal(letra)) {
                    vocalesConsecutivas++;
                    if(vocalesConsecutivas >= 2) {
                        contador++;
                        break;
                    }
                }
                else {
                    vocalesConsecutivas = 0;
                }
            }
        }
        
        System.out.println("Hay " + contador + " palabras con más de dos vocales consecutivas.");
    }
    
    public static boolean esVocal(char letra) {
        return letra == 'a' || letra == 'e' || letra == 'i' || letra == 'o' || letra == 'u';
    }
}
