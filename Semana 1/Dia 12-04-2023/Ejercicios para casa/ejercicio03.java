import java.util.ArrayList;
import java.util.List;

public class ejercicio03 {
    public static void main(String[] args) {
        List<Integer> numeros = new ArrayList<Integer>();
        numeros.add(6);
        numeros.add(7);
        numeros.add(21);
        numeros.add(28);
        numeros.add(55);
        
        int contadorPerfectos = 0;
        int contadorFibonacci = 0;
        
        for(int numero : numeros) {
            if(esNumeroPerfecto(numero)) {
                contadorPerfectos++;
            }
            if(esNumeroFibonacci(numero)) {
                contadorFibonacci++;
            }
        }
        
        System.out.println("Hay " + contadorPerfectos + " números perfectos");
        System.out.println("Hay " + contadorFibonacci + " números de Fibonacci");
    }
    
    public static boolean esNumeroPerfecto(int numero) {
        int sumaDivisores = 0;
        for(int i=1; i<numero; i++) {
            if(numero % i == 0) {
                sumaDivisores += i;
            }
        }
        return sumaDivisores == numero;
    }
    
    public static boolean esNumeroFibonacci(int numero) {
        int a = 0;
        int b = 1;
        while(b < numero) {
            int c = a + b;
            a = b;
            b = c;
        }
        return b == numero;
    }
}
