import java.util.ArrayList;
import java.util.List;

public class ejercicio04 {
    public static void main(String[] args) {
        List<Integer> numeros = new ArrayList<Integer>();
        numeros.add(2520);
        numeros.add(120);
        numeros.add(360);
        numeros.add(840);
        numeros.add(480);
        numeros.add(2160);
        numeros.add(3240);

        int contador = 0;
        
        for(int numero : numeros) {
            if(esDivisiblePorTodosLosNumeros(numero, 10)) {
                contador++;
            }
        }
        
        System.out.println("Hay " + contador + " números divisibles por todos los números de 1 a 10.");
    }
    
    public static boolean esDivisiblePorTodosLosNumeros(int numero, int maximo) {
        for(int i=1; i<=maximo; i++) {
            if(numero % i != 0) {
                return false;
            }
        }
        return true;
    }
}
