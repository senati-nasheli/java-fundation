import java.util.ArrayList;
import java.util.List;

public class ejercicio05 {
    public static void main(String[] args) {
        List<String> palabras = new ArrayList<String>();
        palabras.add("amanecer");
        palabras.add("eucalipto");
        palabras.add("paraguay");
        palabras.add("aceituna");
        palabras.add("secuestro");
        palabras.add("avion");
        palabras.add("estudio");
        
        int contador = 0;
        
        for(String palabra : palabras) {
            if(contieneTodasLasVocales(palabra)) {
                contador++;
            }
        }
        
        System.out.println(contador + " palabras con todas las vocales.");
    }
    
    public static boolean contieneTodasLasVocales(String palabra) {
        return palabra.contains("a") && palabra.contains("e") && palabra.contains("i") && palabra.contains("o") && palabra.contains("u");
    }
}
