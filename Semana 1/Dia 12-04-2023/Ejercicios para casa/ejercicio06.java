import java.util.ArrayList;
import java.util.List;

public class ejercicio06 {
    public static void main(String[] args) {
        List<Integer> numeros = new ArrayList<Integer>();
        numeros.add(2520);
        numeros.add(5040);
        numeros.add(7560);
        numeros.add(232792560);
        numeros.add(15120);

        int maxDivisor = 20;
        boolean encontrado = false;
        
        for(int numero : numeros) {
            encontrado = true;
            for(int i=1; i<=maxDivisor; i++) {
                if(numero % i != 0) {
                    encontrado = false;
                    break;
                }
            }
            if(encontrado) {
                System.out.println(numero + " es divisible por todos los números de 1 a " + maxDivisor + ".");
                break;
            }
        }
        if(!encontrado) {
            System.out.println("No se encontró ningún número que sea divisible por todos los números de 1 a " + maxDivisor + ".");
        }
    }
}
