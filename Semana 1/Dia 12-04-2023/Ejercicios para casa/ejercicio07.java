import java.util.ArrayList;
import java.util.List;

public class ejercicio07 {
    public static void main(String[] args) {
        List<String> palabras = new ArrayList<String>();
        palabras.add("perro");
        palabras.add("corre");
        palabras.add("paseo");
        palabras.add("malla");
        palabras.add("carro");

        int palabrasConRepetidas = 0;

        for(String palabra : palabras) {
            for(int i=0; i<palabra.length()-2; i++) {
                if(palabra.charAt(i) == palabra.charAt(i+1) && palabra.charAt(i) == palabra.charAt(i+2)) {
                    palabrasConRepetidas++;
                    break;
                }
            }
        }

        System.out.println("Hay " + palabrasConRepetidas + " palabras con la misma letra repetida tres veces consecutivas.");
    }
}
