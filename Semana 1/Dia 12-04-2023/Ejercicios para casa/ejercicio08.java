import java.util.ArrayList;
import java.util.List;

public class ejercicio08 {
    public static void main(String[] args) {
        List<Integer> numeros = new ArrayList<Integer>();

        numeros.add(100);
        numeros.add(195);
        numeros.add(911);
        numeros.add(15);
        numeros.add(100);
        numeros.add(3600); 

        int sumaDigitos = 0;

        for (int num : numeros) {
            int temp = num;
            while (temp > 0) {
                sumaDigitos += temp % 10;
                temp /= 10;
            }
        }

        for (int num : numeros) {
            if (num % sumaDigitos == 0) {
                System.out.println(num + " es divisible por la suma de los dígitos de los números de la lista.");
            }
        }
    }
}
