import java.util.ArrayList;
import java.util.List;

public class ejercicio09 {
    public static void main(String[] args) {
        List<String> palabras = new ArrayList<String>();
        palabras.add("casaca");
        palabras.add("calle");
        palabras.add("ventana");
        palabras.add("oso");
        palabras.add("barco");

        int contadorImpares = 0;

        for (String palabra : palabras) {
            char[] letras = palabra.toCharArray();
            for (int i = 0; i < letras.length; i++) {
                char letraActual = letras[i];
                int contadorLetra = 0;
                for (int j = i+1; j < letras.length; j++) {
                    if (letras[j] == letraActual && esVocal(letraActual)) {
                        contadorLetra++;
                    }
                }
                if (contadorLetra % 2 == 1) {
                    contadorImpares++;
                    break; 
                }
            }
        }

        System.out.println("Hay " + contadorImpares + " palabras que tienen una vocal repetida un número impar de veces.");
    }

    private static boolean esVocal(char letra) {
        return letra == 'a' || letra == 'e' || letra == 'i' || letra == 'o' || letra == 'u';
    }
}
