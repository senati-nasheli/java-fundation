import java.util.ArrayList;
import java.util.List;

public class ejercicio11 {
    public static void main(String[] args) {
        List<String> palabras = new ArrayList<String>();
        palabras.add("arroz");
        palabras.add("casa");
        palabras.add("papa");
        palabras.add("salsa");
        palabras.add("mama");

        int contador = 0;

        for (String palabra : palabras) {
            if (palabra.charAt(0) == palabra.charAt(3)) {
                contador++;
            }
        }

        System.out.println("Hay " + contador + " palabras que tienen la misma letra en las posiciones 1 y 4.");
    }
}
