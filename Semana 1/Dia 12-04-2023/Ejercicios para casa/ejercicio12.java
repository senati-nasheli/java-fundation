import java.util.ArrayList;
import java.util.List;

public class ejercicio12 {

    public static void main(String[] args) {
        int count = 0;
        for (int i = 1; i <= 1000; i++) {
            if (isSmithNumber(i)) {
                count++;
            }
        }
        System.out.println("Hay " + count + " números de Smith.");
    }

    private static boolean isSmithNumber(int n) {
        List<Integer> factors = primeFactors(n);
        int sum = 0;
        for (int factor : factors) {
            sum += sumDigits(factor);
        }
        return sumDigits(n) == sum && factors.size() > 1;
    }

    private static List<Integer> primeFactors(int n) {
        List<Integer> factors = new ArrayList<>();
        int factor = 2;
        while (n > 1) {
            if (n % factor == 0) {
                factors.add(factor);
                n /= factor;
            } else {
                factor++;
            }
        }
        return factors;
    }

    private static int sumDigits(int n) {
        int sum = 0;
        while (n > 0) {
            sum += n % 10;
            n /= 10;
        }
        return sum;
    }
}
