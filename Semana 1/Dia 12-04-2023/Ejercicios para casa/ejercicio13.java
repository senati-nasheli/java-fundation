import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ejercicio13 {
    public static void main(String[] args) {
        List<String> palabras = new ArrayList<String>();
        palabras.add("esternocleidomastoideo");
        palabras.add("perpendicular");
        palabras.add("paralelepipedo");
        palabras.add("gallina");
        palabras.add("murcielago");

        int contador = 0;

        for (String palabra : palabras) {
            Map<Character, Integer> frecuenciaLetras = new HashMap<Character, Integer>();
            for (char letra : palabra.toCharArray()) {
                if (frecuenciaLetras.containsKey(letra)) {
                    frecuenciaLetras.put(letra, frecuenciaLetras.get(letra) + 1);
                } else {
                    frecuenciaLetras.put(letra, 1);
                }
            }
            boolean tieneLetraRepetida = false;
            for (char letra : frecuenciaLetras.keySet()) {
                int frecuencia = frecuenciaLetras.get(letra);
                if (frecuencia > 2 && frecuencia < 5) {
                    tieneLetraRepetida = true;
                    break;
                }
            }
            if (tieneLetraRepetida) {
                contador++;
            }
        }

        System.out.println("Hay " + contador + " palabras que tienen una letra que aparece más de dos veces pero menos de cinco veces.");
    }
}
