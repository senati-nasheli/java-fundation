import java.util.ArrayList;

public class ejercicio14 {
    public static void main(String[] args) {
        ArrayList<Integer> numeros = new ArrayList<Integer>();
        numeros.add(100);
        numeros.add(195);
        numeros.add(911);
        numeros.add(15);
        numeros.add(100);
        numeros.add(3600);
        numeros.add(45);
        numeros.add(297);
        numeros.add(703);
        numeros.add(4879);
        numeros.add(5292);

        int contadorKaprekar = 0;

        for (int num : numeros) {
            if (esNumeroKaprekar(num)) {
                contadorKaprekar++;
            }
        }

        System.out.println("Hay " + contadorKaprekar + " números de Kaprekar en la lista.");
    }

    private static boolean esNumeroKaprekar(int num) {
        int cuadrado = num * num;
        String cuadradoStr = String.valueOf(cuadrado);
        int n = cuadradoStr.length();

        for (int i = 1; i < n; i++) {
            int parte1 = Integer.parseInt(cuadradoStr.substring(0, i));
            int parte2 = Integer.parseInt(cuadradoStr.substring(i));
            if (parte1 > 0 && parte2 > 0 && parte1 + parte2 == num) {
                return true;
            }
        }

        return false;
    }
}
