import java.util.Scanner;

public class ejercicio15 {

    public static void main(String[] args) {
        Scanner miau = new Scanner(System.in);
        System.out.print("Ingresa una lista de palabras separadas por comas: ");
        String listaPalabras = miau.nextLine();

        int contador = 0;
        String[] palabras = listaPalabras.split(",");

        for (String palabra : palabras) {
            palabra = palabra.trim(); // Elimina espacios en blanco al inicio y al final de la palabra
            if (palabra.length() > 0 && palabra.charAt(0) == palabra.charAt(palabra.length() - 1)) {
                contador++;
            }
        }

        System.out.println("Hay " + contador + " palabras que tienen la misma letra al principio y al final.");
    }

}
