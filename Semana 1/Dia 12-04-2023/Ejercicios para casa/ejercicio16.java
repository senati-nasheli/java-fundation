import java.util.ArrayList;

public class ejercicio16 {
    public static void main(String[] args) {
        ArrayList<Integer> numeros = new ArrayList<Integer>();
        
        numeros.add(153);
        numeros.add(379);
        numeros.add(371);
        numeros.add(407);
        numeros.add(1548);
        numeros.add(8208);
        numeros.add(9474);
        
        int contadorArmstrong = 0;

        for (int num : numeros) {
            if (esNumeroArmstrong(num)) {
                contadorArmstrong++;
            }
        }

        System.out.println("Hay " + contadorArmstrong + " números de Armstrong en la lista.");
    }

    private static boolean esNumeroArmstrong(int num) {
        int suma = 0;
        int n = String.valueOf(num).length();

        for (int i = 0; i < n; i++) {
            int digito = Integer.parseInt(String.valueOf(num).substring(i, i + 1));
            suma += Math.pow(digito, n);
        }

        return suma == num;
    }
}
