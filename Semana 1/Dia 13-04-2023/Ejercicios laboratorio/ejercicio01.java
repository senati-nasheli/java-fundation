import java.util.Scanner;
import java.lang.Math.*;

public class ejercicio01 {

    public static void main(String[] args) {
        Scanner holaa = new Scanner(System.in);

        System.out.println("Semieje mayor:");
        float a = holaa.nextFloat();

        System.out.println("Semieje menor:");
        float b = holaa.nextFloat();

        double area = Math.PI * a * b;
        double perimetro = Math.PI * (a + b);

        System.out.println("Area: " + area);
        System.out.println("Perimetro: " + perimetro);

    }
}
