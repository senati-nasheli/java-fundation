import java.util.Scanner;

public class ejercicio09 {
    public static void main(String[] args) {
        Scanner holi = new Scanner(System.in);
        
        System.out.println("Lado a:");
        double a = holi.nextDouble();
        System.out.println("Lado b:");
        double b = holi.nextDouble();
        System.out.println("Lado c:");
        double c = holi.nextDouble();

        if(a+b > c && a+c > b && b+c > a) {
            double seno = b / c;
            double coseno = a / c;
            double tangente = b / a;
            System.out.println("Seno: " + b + "/"+ c + " = " + seno);
            System.out.println("Coseno: " + a + "/"+ c + " = "+ coseno);
            System.out.println("Tangente: " + b + "/"+ a + " = " + tangente);
        } else {
            System.out.println("No se forman un triángulo");
        }
    }
}
