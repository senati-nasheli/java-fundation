import java.util.Scanner;

public class ejercicio04 {
    public static void main(String[] args) {
        Scanner uwu = new Scanner(System.in);

        System.out.print("La temperatura en grados centígrados: ");
        double C = uwu.nextDouble();

        double f = (C * 1.8) + 32;

        System.out.println("Son equivalentes a " + f + " grados Fahrenheit.");
    }
}
