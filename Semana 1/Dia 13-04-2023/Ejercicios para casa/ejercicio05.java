import java.util.Scanner;

public class ejercicio05 {
    public static void main(String[] args) {
        Scanner agua = new Scanner(System.in);

        System.out.print("R de la circunferencia: ");
        double r = agua.nextDouble();

        double longitud = 2 * Math.PI * r;
        double area = Math.PI * r * r;

        System.out.println("Longitud: " + longitud);
        System.out.println("Area: " + area);
    }
}
