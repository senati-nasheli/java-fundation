import java.util.Scanner;

public class ejercicio06 {
    public static void main(String[] args) {
        Scanner agua = new Scanner(System.in);

        System.out.print("Introduce la velocidad en Km/h: ");
        double kmh = agua.nextDouble();

        double ms = 1000 / 3600; 
        double msResul = kmh * ms; 

        System.out.println(kmh + " Km/h son equivalentes a " + msResul + " m/s.");
    }
}
