import java.util.Scanner;

public class ejercicio07 {
    public static void main(String[] args) {
        Scanner holii = new Scanner(System.in);

        System.out.print("Primer cateto: ");
        double c1 = holii.nextDouble();

        System.out.print("Segundo cateto: ");
        double c2 = holii.nextDouble();

        double hipotenusa = Math.sqrt(c1 * c1 + c2 * c2);

        System.out.println("La hipotenusa es: " + hipotenusa);
    }
}
