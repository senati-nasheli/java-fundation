import java.util.Scanner;

public class ejercicio08 {
    public static void main(String[] args) {
        Scanner entryxd = new Scanner(System.in);
        System.out.print("Radio de la esfera: ");
        double r = entryxd.nextDouble();

        double volu = 4.0 / 3.0 * Math.PI * Math.pow(r, 3);

        System.out.println("El volumen es: " + volu);
    }
}
