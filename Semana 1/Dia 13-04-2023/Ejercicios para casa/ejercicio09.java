import java.util.Scanner;

public class ejercicio09 {
    public static void main(String[] args) {
        Scanner dbg = new Scanner(System.in);

        System.out.print("Introduce la longitud del primer lado: ");
        double l1 = dbg.nextDouble();

        System.out.print("Introduce la longitud del segundo lado: ");
        double l2 = dbg.nextDouble();

        System.out.print("Introduce la longitud del tercer lado: ");
        double l3 = dbg.nextDouble();

        double s = (l1 + l2 + l3) / 2;
        double area = Math.sqrt(s * (s - l1) * (s - l2) * (s - l3));

        System.out.println("El área del triángulo es: " + area);
    }
}
