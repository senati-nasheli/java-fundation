import java.util.Scanner;

public class ejercicio13 {
    public static void main(String[] args) {
        Scanner ahhh = new Scanner(System.in);
        System.out.print("Fecha de nacimiento (formato dd/mm/aaaa): ");
        String fc = ahhh.nextLine();
        String sinbar = fc.replace("/", "");
        int suma = 0;
        for (int i = 0; i < sinbar.length(); i++) {
            suma += Character.getNumericValue(sinbar.charAt(i));
        }
        while (suma > 9) {
            int sumaParcial = 0;
            while (suma != 0) {
                sumaParcial += suma % 10;
                suma /= 10;
            }
            suma = sumaParcial;
        }
        System.out.println("numero de suerte: " + suma);
    }
}
