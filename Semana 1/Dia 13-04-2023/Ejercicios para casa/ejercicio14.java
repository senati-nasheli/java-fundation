import java.util.Scanner;

public class ejercicio14 {
    public static void main(String[] args) {
        Scanner miau = new Scanner(System.in);
        System.out.print("Ingrese el precio inicial del producto: ");
        double ini = miau.nextDouble();
        System.out.print("Ingrese el porcentaje de impuestos a aplicar: ");
        double impues = miau.nextDouble();
        System.out.print("Ingrese el porcentaje de ganancia deseado: ");
        double gana = miau.nextDouble();
        
        double fhf = ini + (ini * impues / 100) + (ini * gana / 100);
        System.out.println("El precio final de venta del producto es: " + fhf);
    }
}
