import java.util.Scanner;

public class ejercicio16 {
    public static void main(String[] args) {
        Scanner holii = new Scanner(System.in);
        System.out.print("Grados centigrados: ");
        double c = holii.nextDouble();
        
        double k = c + 273.15;
        double reaumur = c * 0.8;
        
        System.out.println("En grados Kelvin es: " + k);
        System.out.println("En grados Réaumur es: " + reaumur);
    }
}
