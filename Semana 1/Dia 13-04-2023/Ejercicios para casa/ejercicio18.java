import java.util.Scanner;

public class ejercicio18 {
    public static void main(String[] args) {
        Scanner uwu = new Scanner(System.in);
        System.out.print("Ingrese un número para mostrar su tabla de multiplicar: ");
        int n = uwu.nextInt();

        for (int i = 1; i <= 10; i++) {
            System.out.println(n + " x " + i + " = " + (n * i));
        }
    }
}
