public class ejercicio03 {
    public static void main(String[] args) {
        if (args.length != 1) {
            System.out.println("ingrese un numero");
            return;
        }
        
        int num = Integer.parseInt(args[0]);
        int fac = 1;
        
        for (int i = 1; i <= num; i++) {
            fac *= i;
        }
        
        System.out.println("fac: " + fac);
    }
}
