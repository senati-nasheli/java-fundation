import java.util.Scanner;
public class ejercicio07 {
    public static void main(String[] args) {
        Scanner awa = new Scanner(System.in);
        System.out.println("numero:");
        int n = awa.nextInt();
        int[] sus = new int[n];
        sus[0] = 1;
        sus[1] = 2;
        for (int i = 2; i < n; i++) {
            int cont = 0;
            for (int j = 0; j < i; j++) {
                for (int k = j + 1; k < i; k++) {
                    if (sus[j] + sus[k] == sus[i - 1]) {
                        cont++;
                    }
                }
            }
            if (cont == 1) {
                sus[i] = sus[i - 1] + 1;
            } else {
                sus[i] = sus[i - 1] + 2;
            }
        }
        System.out.println("sucecion::");
        for (int i = 0; i < n; i++) {
            System.out.print(sus[i] + " ");
        }
    }
}
