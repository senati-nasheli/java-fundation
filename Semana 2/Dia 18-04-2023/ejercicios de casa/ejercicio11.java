public class ejercicio11 {
    public static void main(String[] args) {
        int meses = 20;
        double mensu = 10.0;
        double total = 0.0;
        for (int i = 1; i <= meses; i++) {
            System.out.printf("Mes %d: S/%.2f%n", i, mensu);
            total += mensu;
            mensu *= 2;
        }
        System.out.printf("Total pagado: S/%.2f%n", total);
    }
}
    
