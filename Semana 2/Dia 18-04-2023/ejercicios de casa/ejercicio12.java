import java.util.Scanner;

public class ejercicio12 {
    public static void main(String[] args) {
        Scanner awa = new Scanner(System.in);
        System.out.print("cantidad: ");
        int n = awa.nextInt();
        int conta = 0;
        int num = 2;
        while (conta < n) {
            boolean isPrime = true;
            for (int i = 2; i <= Math.sqrt(num); i++) {
                if (num % i == 0) {
                    isPrime = false;
                    break;
                }
            }
            if (isPrime) {
                System.out.print(num + " ");
                conta++;
            }
            num++;
        }
    }
}
