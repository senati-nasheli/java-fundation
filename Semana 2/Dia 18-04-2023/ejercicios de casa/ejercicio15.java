import java.text.DecimalFormat;

public class ejercicio15 {
    public static void main(String[] args) {
        double saldo = 5000.0;
        double tasa_interes = 0.016;
        int meses = 18;
        DecimalFormat df = new DecimalFormat("#.##");
        System.out.println("saldo inicial: " + df.format(saldo));
        for (int i = 1; i <= meses; i++) {
            saldo = saldo + (saldo * tasa_interes);
            System.out.println("mes " + i + ": " + df.format(saldo));
        }
    }
}
