import java.util.Scanner;

public class ejercicio04 {
    public static void main(String[] args) {
        Scanner alchidk = new Scanner(System.in);
        System.out.print("ingrese un numero ");
        int num1  = alchidk.nextInt();
        int suma = 0;
        int contador = 0;
        int num2 = 1;
        while (contador < num1 ) {
            if (perf(num2)) {
                suma += num2;
                contador++;
            }
            num2++;
        }
        System.out.println("Suma: " + suma);
    }

    public static boolean perf(int n) {
        int sumdiv = 0;
        for (int i = 1; i < n; i++) {
            if (n % i == 0) {
                sumdiv += i;
            }
        }
        return sumdiv == n;
    }
}
