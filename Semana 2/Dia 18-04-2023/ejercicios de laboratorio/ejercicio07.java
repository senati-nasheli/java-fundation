import java.util.Scanner;

public class ejercicio07 {
    public static void main(String[] args) {
        Scanner agua = new Scanner(System.in);
        System.out.print("ingrese un numero: ");
        int num = agua.nextInt();
        int suma = 0;
        int i = 1;
        do {
            if (i % 2 == 0) {
                suma += i;
            }
            i++;
        } while (i <= num);
        System.out.println("SUMA: " + suma);
    }
}
