import java.util.Scanner;

public class ejercicio09 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        System.out.print("Introduce un número entero positivo: ");
        int num = input.nextInt();
        int sum = 0;
        int i = 1;
        do {
            if (num % i == 0) {
                sum += i;
            }
            i++;
        } while (i < num);
        if (sum == num) {
            System.out.println(num + " es un número perfecto.");
        } else {
            System.out.println(num + " no es un número perfecto.");
        }
    }
}
