import java.util.Scanner;
public class ejercicio01 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.print("cuantos numeros ingresara? ");
        int n = sc.nextInt();
        int suma = sumarnum(n, sc);
        System.out.println("La suma es: " + suma);
    }

    public static int sumarnum(int n, Scanner sc) {
        int[] arreglo = new int[n];
        int suma = 0;

        for (int i = 0; i < n; i++) {
            System.out.print("numero: " + (i + 1) + ": ");
            arreglo[i] = sc.nextInt();
            suma += arreglo[i];
        }

        return suma;
    }
}

