import java.util.Scanner;

public class ejercicio02 {
    public static void main(String[] args) {
        Scanner awa = new Scanner(System.in);
        System.out.print("cuantos numeros ingresara? ");
        int n = awa.nextInt();
        int multi = resultado(n, awa);
        System.out.println("resultado: " + multi);

    }

    public static int resultado(int n, Scanner awa) {
        int[] operacion = new int[n];
        int multi = 1;

        for (int i = 0; i < n; i++) {
            System.out.print("numero: " + (i + 1) + ": ");
            operacion[i] = awa.nextInt();
            multi *= operacion[i];
        }

        return multi;
    }
}
