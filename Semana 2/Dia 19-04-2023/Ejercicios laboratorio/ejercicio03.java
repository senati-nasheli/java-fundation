import java.util.Scanner;

public class ejercicio03 {
    public static void main(String[] args) {
        Scanner awa = new Scanner(System.in);
        System.out.print("numero de estudiantes: ");
        int n = awa.nextInt();
        String[][] estus = new String[n][2];

        for (int i = 0; i < n; i++) {
            System.out.print("nombre " + (i + 1) + ": ");
            estus[i][0] = awa.next();
            System.out.print("edad de " + estus[i][0] + ": ");
            estus[i][1] = awa.next();
        }

        double promedio = calculo(estus);
        System.out.println("promedio de edades: " + promedio);
    }

    public static double calculo(String[][] estus) {
        int suma = 0;
        for (int i = 0; i < estus.length; i++) {
            suma += Integer.parseInt(estus[i][1]); 
        }
        return (double) suma / estus.length;
    }
}
