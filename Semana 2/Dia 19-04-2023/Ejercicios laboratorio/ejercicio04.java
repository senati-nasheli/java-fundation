import java.util.Scanner;
public class ejercicio04 {
    public static void main(String[] args) {
        Scanner awa = new Scanner(System.in);
        System.out.print("cuantos numeros seran? ");
        int n = awa.nextInt();
        int[] num = new int[n];
        for (int i = 0; i < n; i++) {
            System.out.print("numero" + (i + 1) + ": ");
            num[i] = awa.nextInt();
        }
        int maxi = busca(num);
        System.out.println("maximo: " + maxi);
    }

    public static int busca(int[] num) {
        int maxi = num[0];

        for (int i = 1; i < num.length; i++) {
            if (num[i] > maxi) {
                maxi = num[i];
            }
        }
        return maxi;
    }
}
