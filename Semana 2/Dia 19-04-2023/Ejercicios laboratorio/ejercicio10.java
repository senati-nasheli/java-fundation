import java.util.Scanner;

public class ejercicio10 {
    public static void main(String[] args) {
        Scanner awa = new Scanner(System.in);
        System.out.print("cuantos numeros ingresara? ");
        int n = awa.nextInt();
        int[] conta = new int[n];

        for (int i = 0; i < n; i++) {
            System.out.print("numero:" + (i+1) + ": ");
            conta[i] = awa.nextInt();
        }
        orde(conta);
        System.out.print("en forma descendente: ");
        for (int i = 0; i < n; i++) {
            System.out.print(conta[i] + " - ");
        }
    }

    public static void orde(int[] conta) {
        int n = conta.length;
        for (int i = 0; i < n-1; i++) {
            for (int j = i+1; j < n; j++) {
                if (conta[i] < conta[j]) {
                    int temp = conta[i];
                    conta[i] = conta[j];
                    conta[j] = temp;
                }
            }
        }
    }
}
