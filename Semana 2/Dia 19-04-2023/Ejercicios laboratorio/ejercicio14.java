import java.util.Scanner;

public class ejercicio14 {
    public static void main(String[] args) {
        Scanner awa = new Scanner(System.in);
        System.out.print("Ingresa un numero: ");
        int num = awa.nextInt();
        int[] dgt = new int[4];
        int i = 0;
        while (num > 0) {
            dgt[i] = num % 10;
            num /= 10;
            i++;
        }
        for (int a = dgt.length - 1; a >= 0; a--) {
            System.out.println("[" + a + "] = " + dgt[a]);
        }
    }
}
