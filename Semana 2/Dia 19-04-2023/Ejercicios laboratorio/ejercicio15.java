import java.util.Scanner;

public class ejercicio15 {
    
    public static void main(String[] args) {
        Scanner awa = new Scanner(System.in);
        System.out.print("numero a convertir: ");
        int num = awa.nextInt();

        System.out.println("En numero romano:" + conver(num));
    }

    public static String conver(int numero) {
        String[] roma = {"M", "CM", "D", "CD", "C", "XC", "L", "XL", "X", "IX", "V", "IV", "I"};
        int[] vals = {1000, 900, 500, 400, 100, 90, 50, 40, 10, 9, 5, 4, 1};
        String romano = "";

        for (int i = 0; i < vals.length; i++) {
            while (numero >= vals[i]) {
                romano += roma[i];
                numero -= vals[i];
            }
        }
        return romano;
    }
}
