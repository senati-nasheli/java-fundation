import java.util.Scanner;

public class ejercicio16 {

    public static void main(String[] args) {
        String[] nom = new String[100];
        int[] eda = new int[100];
        int conta = 0;
        conta = alum(nom, eda);
        System.out.println("\nmayores de edad:");
        for (int i = 0; i < conta; i++) {
            if (eda[i] >= 18) {
                System.out.println(nom[i] + " - " + eda[i] + " años");
            }
        }

        int mayor = mayorob(eda, conta);
        System.out.println("\nAlumnos con mayor edad (" + mayor + " años):");
        for (int i = 0; i < conta; i++) {
            if (eda[i] == mayor) {
                System.out.println(nom[i]);
            }
        }
    }

    public static int alum(String[] nom, int[] eda) {
        Scanner aea = new Scanner(System.in);
        String nombre;
        int edad;
        int conta = 0;
        System.out.println("nombre y edad (introduce * para terminar):");
        do {
            System.out.print("nombre: ");
            nombre = aea.nextLine();
            if (!nombre.equals("*")) {
                System.out.print("edad: ");
                edad = aea.nextInt();
                aea.nextLine(); 
                nom[conta] = nombre;
                eda[conta] = edad;
                conta++;
            }
        } while (!nombre.equals("*"));
        return conta;
    }

    public static int mayorob(int[] eda, int conta) {
        int mayor = 0;
        for (int i = 0; i < conta; i++) {
            if (eda[i] > mayor) {
                mayor = eda[i];
            }
        }
        return mayor;
    }
}
