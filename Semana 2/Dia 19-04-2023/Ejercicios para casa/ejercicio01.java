public class ejercicio01 {
    public static void main(String[] args) {
        int[][] matri = new int[7][7];

        for (int i = 0; i < matri.length; i++) {
            for (int j = 0; j < matri[0].length; j++) {
                matri[i][j] = i * matri.length + j + 1;
            }
        }
        System.out.println("matri:");
        matriz(matri);
        int[][] rotando = rotando(matri);
        System.out.println("rotada 90 grados:");
        matriz(rotando);
    }

    public static int[][] rotando(int[][] matri) {
        int[][] rotando = new int[matri[0].length][matri.length];
        for (int i = 0; i < matri.length; i++) {
            for (int j = 0; j < matri[0].length; j++) {
                rotando[j][matri.length - i - 1] = matri[i][j];
            }
        }
        return rotando;
    }

    public static void matriz(int[][] matri) {
        for (int i = 0; i < matri.length; i++) {
            for (int j = 0; j < matri[0].length; j++) {
                System.out.print(matri[i][j] + "\t");
            }
            System.out.println();
        }
    }
}
