import java.util.Random;

public class ejercicio02 {
    public static void main(String[] args) {
        int[][] mat = new int[7][7];

        Random num = new Random();
        for (int i = 0; i < mat.length; i++) {
            for (int j = 0; j < mat[0].length; j++) {
                mat[i][j] = num.nextInt(100) + 1;
            }
        }
        result(mat);
    }

    public static void result(int[][] mat) {
        for (int i = 0; i < mat.length; i++) {
            for (int j = 0; j < mat[0].length; j++) {
                System.out.print(mat[i][j] + "\t");
            }
            System.out.println();
        }
    }
}
