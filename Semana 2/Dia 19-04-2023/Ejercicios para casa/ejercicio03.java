public class ejercicio03 {
    public static void main(String[] args) {
        int[][] mat1 = new int[5][5];
        int[][] mat2 = new int[][] {{1, 2, 3}, {4, 5, 6}, {7, 8, 9}};
        for (int i = 0; i < mat1.length; i++) {
            for (int j = 0; j < mat1[0].length; j++) {
                mat1[i][j] = i * mat1.length + j + 1;
            }
        }
        System.out.println("matriz 1:");
        matriz(mat1);
        System.out.println("matriz 2:");
        matriz(mat2);
        int[][] resultado = multiplicarMatrices(mat1, mat2);
        System.out.println("multiplicando:");
        matriz(resultado);
    }

    public static int[][] multiplicarMatrices(int[][] mat1, int[][] mat2) {
        int f1 = mat1.length;
        int g1 = mat1[0].length;
        int f2 = mat2.length;
        int g2 = mat2[0].length;

        if (g1 != f2) {
            throw new IllegalArgumentException("Las matrices no son multiplicables");
        }

        int[][] resultado = new int[f1][g2];

        for (int i = 0; i < f1; i++) {
            for (int j = 0; j < g2; j++) {
                int sum = 0;
                for (int k = 0; k < g1; k++) {
                    sum += mat1[i][k] * mat2[k][j];
                }
                resultado[i][j] = sum;
            }
        }

        return resultado;
    }

    public static void matriz(int[][] matriz) {
        for (int i = 0; i < matriz.length; i++) {
            for (int j = 0; j < matriz[0].length; j++) {
                System.out.print(matriz[i][j] + "\t");
            }
            System.out.println();
        }
    }
}
