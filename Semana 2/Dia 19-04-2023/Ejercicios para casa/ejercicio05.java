import java.util.Scanner;

public class ejercicio05 {

    public static void main(String[] args) {
        Scanner awa = new Scanner(System.in);
        System.out.print("tamaño o longitud: ");
        int tam = awa.nextInt();
        int[] ope = new int[tam];
        System.out.println("numeros::");

        for (int i = 0; i < tam; i++) {
            ope[i] = awa.nextInt();
        }
        int[] subope = encontrando(ope);
        System.out.print("orden creciente: ");
        for (int i = 0; i < subope.length; i++) {
            System.out.print(subope[i] + " ");
        }
    }

    public static int[] encontrando(int[] ope) {
        int inicio = 0;
        int fin = 0;
        int tamActual = 1;
        int tamMaxima = 1;
        for (int i = 1; i < ope.length; i++) {
            if (ope[i] > ope[i-1]) {
                tamActual++;
                if (tamActual > tamMaxima) {
                    tamMaxima = tamActual;
                    fin = i;
                    inicio = fin - tamMaxima + 1;
                }
            } else {
                tamActual = 1;
            }
        }

        int[] subope = new int[tamMaxima];
        for (int i = inicio; i <= fin; i++) {
            subope[i - inicio] = ope[i];
        }
        return subope;
    }
}
