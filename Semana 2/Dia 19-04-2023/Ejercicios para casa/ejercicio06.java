import java.util.Arrays;
import java.util.Collections;

public class ejercicio06 {
    public static void main(String[] args) {
        String[] arreglo = {"manzana", "platano", "naranja", "kiwi", "piña"};
        System.out.println("normal " + Arrays.toString(arreglo));

        Arrays.sort(arreglo, Collections.reverseOrder());
        System.out.println("en orden lexicográfico inverso: " + Arrays.toString(arreglo));
    }
}
