import java.util.Arrays;
import java.util.Scanner;

public class ejercicio07 {
    public static void main(String[] args) {
        Scanner awa = new Scanner(System.in);
        System.out.print("cuantos numeros ingresara? ");
        int n = awa.nextInt();

        int[] arreglo = new int[n];
        for (int i = 0; i < n; i++) {
            System.out.print("numero " + (i+1) + ": ");
            arreglo[i] = awa.nextInt();
        }

        System.out.print("valor de k: ");
        int k = awa.nextInt();
        int kgrande = encontrando(arreglo, k);

        System.out.println("El " + k + "-ésimo elemento mas grande es: " + kgrande);
    }

    public static int encontrando(int[] arreglo, int k) {
        Arrays.sort(arreglo);
        return arreglo[arreglo.length - k];
    }
}
