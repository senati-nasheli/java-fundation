import java.util.Arrays;
import java.util.Scanner;

public class ejercicio09 {
    public static void main(String[] args) {
        Scanner awa = new Scanner(System.in);
        System.out.print("cuantos numeros ingresara? ");
        int n = awa.nextInt();

        int[] num = new int[n];
        for (int i = 0; i < n; i++) {
            System.out.print("numero " + (i+1) + ": ");
            num[i] = awa.nextInt();
        }

        double mediana = encontrando(num);

        System.out.println("La mediana es: " + mediana);
    }

    public static double encontrando(int[] num) {
        Arrays.sort(num);
        int n = num.length;
        if (n % 2 == 0) {
            return (double) (num[n/2 - 1] + num[n/2]) / 2.0;
        } else {
            return (double) num[n/2];
        }
    }
}
