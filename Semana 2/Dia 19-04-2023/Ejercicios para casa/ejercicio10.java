import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

public class ejercicio10 {
    public static void main(String[] args) {
        Scanner awa = new Scanner(System.in);
        System.out.print("cuantos numeros ingresara? ");
        int n = awa.nextInt();

        int[] num = new int[n];
        for (int i = 0; i < n; i++) {
            System.out.print("numero " + (i+1) + ": ");
            num[i] = awa.nextInt();
        }

        int moda = encontrarModa(num);

        System.out.println("La moda es: " + moda);
    }

    public static int encontrarModa(int[] num) {
        Map<Integer, Integer> tot = new HashMap<>();

        for (int i = 0; i < num.length; i++) {
            int valor = num[i];
            if (tot.containsKey(valor)) {
                tot.put(valor, tot.get(valor) + 1);
            } else {
                tot.put(valor, 1);
            }
        }

        int maxi = 0;
        int moda = 0;
        for (Map.Entry<Integer, Integer> entry : tot.entrySet()) {
            int valor = entry.getKey();
            int frecuencia = entry.getValue();
            if (frecuencia > maxi) {
                maxi = frecuencia;
                moda = valor;
            }
        }

        return moda;
    }
}
