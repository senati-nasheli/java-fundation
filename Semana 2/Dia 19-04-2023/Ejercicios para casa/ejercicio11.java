import java.util.Scanner;

public class ejercicio11 {
    public static void main(String[] args) {
        Scanner awa = new Scanner(System.in);
        System.out.print("cuantos numeros ingresara? ");
        int n = awa.nextInt();

        int[] nums = new int[n];
        for (int i = 0; i < n; i++) {
            System.out.print("numero " + (i+1) + ": ");
            nums[i] = awa.nextInt();
        }

        double media = encontrar(nums);

        System.out.println("La media es: " + media);
    }

    public static double encontrar(int[] nums) {
        double sum = 0;
        for (int i = 0; i < nums.length; i++) {
            sum += nums[i];
        }
        return sum / nums.length;
    }
}
