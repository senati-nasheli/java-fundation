import java.util.Scanner;

public class ejercicio13 {
    public static void main(String[] args) {
        Scanner awa = new Scanner(System.in);
        System.out.print("cuantos numeros ingresara? ");
        int n = awa.nextInt();

        int[] nums = new int[n];
        for (int i = 0; i < n; i++) {
            System.out.print("numero " + (i+1) + ": ");
            nums[i] = awa.nextInt();
        }

        int contando = 0;
        for (int i = 0; i < nums.length; i++) {
            if (nums[i] % 2 != 0) {
                contando++;
            }
        }

        System.out.println("Hay " + contando + " números impares.");
    }
}
