import java.util.Arrays;
import java.util.HashMap;
import java.util.Scanner;

public class ejercicio16 {
    public static void main(String[] args) {
        Scanner awa = new Scanner(System.in);
        System.out.print("cuantos números ingresara? ");
        int n = awa.nextInt();
        
        int[] numeros = new int[n];
        for (int i = 0; i < n; i++) {
            System.out.print("numero " + (i+1) + ": ");
            numeros[i] = awa.nextInt();
        }

        int masrepe = encontrando(numeros);
        System.out.println("el que mas se repite " + masrepe);
    }

    public static int encontrando(int[] numeros) {
        HashMap<Integer, Integer> conteo = new HashMap<Integer, Integer>();

        for (int numero : numeros) {
            if (conteo.containsKey(numero)) {
                conteo.put(numero, conteo.get(numero) + 1);
            } else {
                conteo.put(numero, 1);
            }
        }

        int masrepe = numeros[0];
        int maximo = 1;

        for (int numero : conteo.keySet()) {
            if (conteo.get(numero) > maximo) {
                masrepe = numero;
                maximo = conteo.get(numero);
            }
        }

        return masrepe;
    }
}
