import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.Arrays;

public class ejercicio02 {

    public static int calculando(int[] numeros) {
        if (numeros.length == 2) {
            return calculando(numeros[0], numeros[1]);
        } else {
            int num1 = numeros[0];
            int[] numss = Arrays.copyOfRange(numeros, 1, numeros.length);
            int mcdResto = calculando(numss);
            return calculando(num1, mcdResto);
        }
    }

    public static int calculando(int a, int b) {
        if (b == 0) {
            return a;
        } else {
            return calculando(b, a % b);
        }
    }

    public static int[] leerArchivo(String archivo) {
        try (BufferedReader br = new BufferedReader(new FileReader(archivo))) {
            String linea = br.readLine();
            String[] numerosStr = linea.split("\\s+");
            int[] numeros = new int[numerosStr.length];
            for (int i = 0; i < numerosStr.length; i++) {
                numeros[i] = Integer.parseInt(numerosStr[i]);
            }
            return numeros;
        } catch (IOException e) {
            System.out.println("Error al leer el archivo " + archivo);
            return new int[0];
        }
    }

    public static void main(String[] args) {
        String archivo = "aleatorios.txt";
        int[] numeros = leerArchivo(archivo);
        int mcd = calculando(numeros);
        System.out.printf("MCD de %s es %d", Arrays.toString(numeros), mcd);
    }
}
