import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

public class ejercicio04 {
    public static void main(String[] args) {
        String archivo = "aleatorios.txt";
        int[] numeros = leerArchivo(archivo);
        int n = numeros.length;
        long resultado = calcularFactorial(n);
        System.out.println("El número de permutaciones es: " + resultado);
    }

    public static int[] leerArchivo(String archivo) {
        int[] numeros = null;
        try (BufferedReader br = new BufferedReader(new FileReader(archivo))) {
            String linea = br.readLine();
            String[] numerosString = linea.split(" ");
            numeros = new int[numerosString.length];
            for (int i = 0; i < numerosString.length; i++) {
                numeros[i] = Integer.parseInt(numerosString[i]);
            }
        } catch (IOException e) {
            System.out.println("Error al leer el archivo " + archivo);
        }
        return numeros;
    }

    public static long calcularFactorial(int n) {
        if (n == 0) {
            return 1;
        } else {
            return n * calcularFactorial(n - 1);
        }
    }
}
