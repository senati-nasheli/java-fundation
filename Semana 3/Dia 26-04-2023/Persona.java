public class Persona {
    String nombre;
    String apellidos;
    String númeroDocumentoIdentidad;
    int añoNacimiento;
    String paísNacimiento;
    char género;

    Persona(String nombre, String apellidos, String númeroDocumentoIdentidad, int añoNacimiento, String paísNacimiento,
            char género) {
        this.nombre = nombre;
        this.apellidos = apellidos;
        this.númeroDocumentoIdentidad = númeroDocumentoIdentidad;
        this.añoNacimiento = añoNacimiento;
        this.paísNacimiento = paísNacimiento;
        this.género = género;
    }

    public void imprimir() {
        System.out.println("Nombre: " + nombre);
        System.out.println("Apellidos: " + apellidos);
        System.out.println("Número de Documento de Identidad: " + númeroDocumentoIdentidad);
        System.out.println("Año de nacimiento: " + añoNacimiento);
        System.out.println("País de nacimiento: " + paísNacimiento);
        System.out.println("Género: " + género);
    }

    public static void main(String[] args) {
        Persona persona1 = new Persona("Laura", "Gómez", "074515126", 1992, "Argentina", 'M');
        Persona persona2 = new Persona("Carlos", "Sánchez", "90849812", 1985, "Colombia", 'H');
        persona1.imprimir();
        System.out.println("============");
        persona2.imprimir();
    }
}
