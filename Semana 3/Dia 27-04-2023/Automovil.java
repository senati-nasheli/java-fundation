
public class Automovil {
    String marca;
    int modelo;
    boolean automatico;
    int motor;

    enum tipoCom {
        GASOLINA, BIOETANOL, DIESEL, BIODISESEL, GAS_NATURAL
    }

    tipoCom tipoCombustible;

    enum tipoA {
        CIUDAD, SUBCOMPACTO, COMPACTO, FAMILIAR, EJECUTIVO, SUV
    }

    tipoA tipoAutomovil;
    int numeroPuertas;
    int cantidadAsientos;
    int velocidadMaxima;

    enum tipoColor {
        BLANCO, NEGRO, ROJO, NARANJA, AMARILLO, VERDE, AZUL, VIOLETA
    }

    tipoColor color;
    int velocidadActual = 0;
    int contadorMultas = 0;
    

    Automovil(String marca, int modelo, int motor, tipoCom tipoCombustible, tipoA tipoAutomovil, int numeroPuertas, int cantidadAsientos, int velocidadMaxima, tipoColor color,
            boolean automatico) {
        this.marca = marca;
        this.modelo = modelo;
        this.motor = motor;
        this.tipoCombustible = tipoCombustible;
        this.tipoAutomovil = tipoAutomovil;
        this.numeroPuertas = numeroPuertas;
        this.cantidadAsientos = cantidadAsientos;
        this.velocidadMaxima = velocidadMaxima;
        this.color = color;
        this.automatico = automatico;
        this.velocidadActual = 0;
        this.contadorMultas = 0;

    }

    String getMarca() {
        return marca;
    }

    int getModelo() {
        return modelo;
    }

    int getMotor() {
        return motor;
    }

    tipoCom getTipoCombustible() {
        return tipoCombustible;
    }

    tipoA getTipoAutomovil() {
        return tipoAutomovil;
    }

    int getNumeroPuertas() {
        return numeroPuertas;
    }

    int getCantidadAsientos() {
        return cantidadAsientos;
    }
    
    int getVelocidadMaxima() {
        return velocidadMaxima;
    }
  
    tipoColor getColor() {
        return color;
    }
    
  
    int getVelocidadActual() {
        return velocidadActual;
    }
    
    boolean getAutomatico() {
        return automatico;
    }

    int getContadorMultas() {
        return contadorMultas;
    }

    void setAutomatico(boolean automatico) {
    this.automatico = automatico;
    }

    void setMarca(String marca) {
        this.marca = marca;
    }
    
    void setModelo(int modelo) {
        this.modelo = modelo;
    }
   
    void setMotor(int motor) {
        this.motor = motor;
    }

    void setTipoCombustible(tipoCom tipoCombustible) {
        this.tipoCombustible = tipoCombustible;
    }
    
    void setTipoAutomovil(tipoA tipoAutomovil) {
        this.tipoAutomovil = tipoAutomovil;
    }
    
    void setNumeroPuertas(int numeroPuertas) {
        this.numeroPuertas = numeroPuertas;
    }
    
    void setCantidadAsientos(int cantidadAsientos) {
        this.cantidadAsientos = cantidadAsientos;
    }

    void setVelocidadMaxima(int velocidadMaxima) {
        this.velocidadMaxima = velocidadMaxima;
    }

    void setColor(tipoColor color) {
        this.color = color;
    }

    void setVelocidadActual(int velocidadActual) {
        this.velocidadActual = velocidadActual;
    }

    void setContadorMultas(int contadorMultas) {
        this.contadorMultas = contadorMultas;
    }

    void acelerar(int cantidad) {
        int nuevaVelocidad = this.velocidadActual + cantidad;
        if (nuevaVelocidad > this.velocidadMaxima) {
            System.out.println("Tienes una multa por exceso de velocidad");
            this.contadorMultas++;
        }
        this.velocidadActual = Math.min(nuevaVelocidad, this.velocidadMaxima);
    }

    void desacelerar(int decrementoVelocidad) {
        if ((velocidadActual - decrementoVelocidad) > 0) {
            velocidadActual = velocidadActual - decrementoVelocidad;
        } else {
            System.out.println("No se puede decrementar a una velocidad negativa.");
        }
    }

    void frenar(int cantidad) {
        if (this.velocidadActual >= cantidad) {
            int nuevaVelocidad = this.velocidadActual - cantidad;
            this.velocidadActual = nuevaVelocidad;
        } else {
            this.velocidadActual = 0;
        }
    }

    double calcularTiempoLlegada(int distancia) {
        return distancia/velocidadActual;
    }

    void imprimir() {
        System.out.println("Marca = "  + marca);
        System.out.println("Modelo = "  + modelo);
        System.out.println("Motor = "  + motor);
        System.out.println("Tipo de combustible = "  + tipoCombustible);
        System.out.println("Tipo de automóvil = "  + tipoAutomovil);
        System.out.println("Número de puertas = "  + numeroPuertas);
        System.out.println("Cantidad de asientos = " + cantidadAsientos);
        System.out.println("Velocida máxima = "  + velocidadMaxima);
        System.out.println("Color = "  + color);
        System.out.println("Automatico = " + automatico);
    }

    public static void main(String args[]) {
Automovil auto1 = new Automovil("Ford", 2022, 2, Automovil.tipoCom.GASOLINA, Automovil.tipoA.SUV, 4, 5, 200, Automovil.tipoColor.AZUL, true);
        auto1.imprimir();
        auto1.setVelocidadActual(100);
        System.out.println("Velocidad actual = " + auto1.velocidadActual);
        auto1.acelerar(300);
        System.out.println("Velocidad actual = " + auto1.velocidadActual);
        auto1.desacelerar(50);
        System.out.println("Velocidad actual = " + auto1.velocidadActual);
        auto1.frenar(20);
        System.out.println("Velocidad actual = " + auto1.velocidadActual);
        auto1.acelerar(300);
        System.out.println("Velocidad actual = " + auto1.velocidadActual);
        auto1.desacelerar(150);
        System.out.println("Velocidad actual = " + auto1.velocidadActual);
        auto1.acelerar(60);
        System.out.println("Velocidad actual = " + auto1.velocidadActual);
        auto1.acelerar(100);
        System.out.println("Contador de multas: " + auto1.contadorMultas);

    }
}
