public class Rombo {
    int lado;
    int diame1;
    int diame2;

    Rombo(int lado, int diame1, int diame2) {
        this.lado = lado;
        this.diame1 = diame1;
        this.diame2 = diame2;
    }

    double calcularArea() {
        return (diame1 * diame2) / 2;
    }

    double calcularPerimetro() {
        return 4 * lado;
    }
}
