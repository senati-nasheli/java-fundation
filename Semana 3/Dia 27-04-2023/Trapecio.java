public class Trapecio {
    int altura;
    int base1;
    int base2;

    Trapecio(int altura, int base1, int base2) {
        this.altura = altura;
        this.base1 = base1;
        this.base2 = base2;
    }

    double calcularArea() {
        return ((base1 + base2) / 2)*altura;
    }

    double calcularPerimetro() {
        return base1 + base2;
    }
}
